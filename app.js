const express = require("express");
const { get } = require("lodash");

// express app
const app = express();

// listen for requests
app.listen(3000);

// app.get("/", (req, res) => {
//   res.send(`
//     <div>
//       <h1>Home page</h1>
//       <p>This is awesome tutorial with net ninja!</p>
//     </div>
//   `);
// });

app.get("/", (req, res) => {
  res.sendFile("./views/index.html", { root: __dirname });
});

app.get("/about", (req, res) => {
  res.sendFile("./views/about.html", { root: __dirname });
});

// redirects
app.get("/home", (req, res) => {
  res.redirect("/");
});

app.get("/about-us", (req, res) => {
  res.redirect("about");
});

// 404 page
app.use((req, res) => {
  res.status(404).sendFile("./views/404.html", { root: __dirname });
});
